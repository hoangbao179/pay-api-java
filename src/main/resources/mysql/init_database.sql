CREATE TABLE IF NOT EXISTS product (
   id bigint NOT NULL AUTO_INCREMENT,
   name varchar(50) DEFAULT NULL,
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='product entity';

INSERT INTO product (name) VALUES
                          ('Apple'),
                          ('Orange'),
                          ('Potato');
