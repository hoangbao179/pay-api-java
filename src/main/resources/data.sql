CREATE TABLE IF NOT EXISTS product (
                                   id bigint NOT NULL AUTO_INCREMENT,
                                   name varchar(50) DEFAULT NULL,
    PRIMARY KEY (id)
    );

INSERT INTO product (name) VALUES
                               ('Apple'),
                               ('Orange'),
                               ('Potato');