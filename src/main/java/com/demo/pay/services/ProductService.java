package com.demo.pay.services;

import com.demo.pay.domain.Product;
import com.demo.pay.dto.ProductRequest;
import com.demo.pay.dto.ProductResponse;
import com.demo.pay.mapper.IProductMapper;
import com.demo.pay.repository.IProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductService implements IProductService {

    private final IProductRepository productRepository;
    private final IProductMapper productMapper;

    @Override
    public List<ProductResponse> findAll() {
        return productMapper.toResponse(productRepository.findAll());
    }

    @Override
    public ProductResponse findById(Long id) {
        return productMapper.toResponse(productRepository.findById(id).get());
    }

    @Override
    public ProductResponse createOrUpdate(Long id, ProductRequest request) {
        if (id != 0) {
            Product product = productRepository.findById(id).get();
            product.setName(request.getName());
            return productMapper.toResponse(productRepository.save(product));
        }
        Product product = new Product();
        product.setName(request.getName());
        return productMapper.toResponse(productRepository.save(product));
    }

    @Override
    public void delete(Long id) {
        Product product = productRepository.findById(id).get();
        productRepository.delete(product);
    }
}
