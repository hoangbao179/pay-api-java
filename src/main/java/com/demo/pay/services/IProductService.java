package com.demo.pay.services;

import com.demo.pay.dto.ProductRequest;
import com.demo.pay.dto.ProductResponse;

import java.util.List;

public interface IProductService {

    List<ProductResponse> findAll();

    ProductResponse findById(Long id);

    ProductResponse createOrUpdate(Long id, ProductRequest request);

    void delete(Long id);
}
