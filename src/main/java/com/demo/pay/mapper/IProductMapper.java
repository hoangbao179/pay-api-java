package com.demo.pay.mapper;

import com.demo.pay.domain.Product;
import com.demo.pay.dto.ProductResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IProductMapper {
    ProductResponse toResponse(Product product);
    List<ProductResponse> toResponse(List<Product> product);
}
