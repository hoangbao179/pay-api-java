package com.demo.pay.controller;

import com.demo.pay.dto.ProductRequest;
import com.demo.pay.dto.ProductResponse;
import com.demo.pay.services.IProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    protected IProductService productService;

    public ProductController(IProductService productService){
        this.productService = productService;

    }

    @GetMapping()
    public ResponseEntity<List<ProductResponse>> findAll(){
        return ResponseEntity.ok().body(productService.findAll());
    }

    @GetMapping("/id")
    public ResponseEntity<ProductResponse> findById(@PathVariable Long id){
        return ResponseEntity.ok().body(productService.findById(id));
    }

    @PostMapping("/id")
    public ResponseEntity<ProductResponse> createOrUpdate(@PathVariable Long id, ProductRequest request){
        return ResponseEntity.ok().body(productService.createOrUpdate(id, request));
    }

    @DeleteMapping("/id")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
